/**
 * Copyright (c) 2014 Janua. All Rights Reserved
 * Author : Daly Chikhaoui <dchikhaoui@janua.fr>
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at legal-notices/CDDLv1_0.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information:
 *      Portions Copyright [yyyy] [name of copyright owner]
 */

package org.janua;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AbstractPromptReceiver;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.JsonParser;
import com.google.api.client.json.JsonToken;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.MemoryDataStoreFactory;
import com.google.api.services.admin.directory.Directory;
import com.google.api.services.admin.directory.model.User;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.digest.DigestUtils;

public class GooglePassword {

	private static String CLIENT_REFRESH = "google_config.json";

	private static String REDIRECT_URI = "urn:ietf:wg:oauth:2.0:oob";
	
	private static final List<String> SCOPES = Arrays.asList("https://www.googleapis.com/auth/admin.directory.user");
	
	private static final JsonFactory JSON_FACTORY = new JacksonFactory();

	private static final HttpTransport HTTP_TRANSPORT;
	static {
		NetHttpTransport t = null;
		try {
			t = GoogleNetHttpTransport.newTrustedTransport();
		} catch (Exception e) {
			try {
				t = new NetHttpTransport.Builder().doNotValidateCertificate().build();
			} catch (GeneralSecurityException e1) {
				
			}
		}
		HTTP_TRANSPORT = t;
	}

	
	public static void main(String[] args) throws Exception {
		
		if (args.length == 1) {
			
			File clientJson = new File(args[0]);
			
			if (clientJson.exists() && clientJson.isFile()) {
				Map<String, Object> googleConfig = getGoogleConfig(clientJson);
				
				System.out.println(JSON_FACTORY.toPrettyString(googleConfig));
				
				File refreshJson = new File(CLIENT_REFRESH);
				refreshJson.createNewFile();
				FileWriter refreshJsonFW = new FileWriter(refreshJson);
				refreshJsonFW.write(JSON_FACTORY.toPrettyString(googleConfig));
				refreshJsonFW.flush();
				refreshJsonFW.close();
			}
			
		} else {
		
			String fileName = new File(GooglePassword.class.getProtectionDomain().getCodeSource().getLocation().toURI()).getName();
			System.out.println("Usage: java -jar " + fileName + " <path to client_secrets.json>");
		}
	}
	
	
	static Map<String, Object> getGoogleConfig (File clientJson) throws IOException, URISyntaxException {
		
		System.setProperty("https.protocols", "SSLv3");
		
		GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new FileReader(clientJson));
		
		GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES)
												.setAccessType("offline")
												.setApprovalPrompt("force")
												.setDataStoreFactory(MemoryDataStoreFactory.getDefaultInstance())
												.build();
		
		Credential credential = new AuthorizationCodeInstalledApp(flow, new AbstractPromptReceiver() {
																					public String getRedirectUri() throws IOException {
																						return REDIRECT_URI;
																			}
																		}).authorize("user");
		
		LinkedHashMap<String, Object> configMap = new LinkedHashMap<String, Object>(
				3);
		configMap.put("clientId", clientSecrets.getDetails().getClientId());
		configMap.put("clientSecret", clientSecrets.getDetails()
				.getClientSecret());
		configMap.put("refreshToken", credential.getRefreshToken());
		return configMap;
	}

	
	public static void changePassword(String user, String password) throws Exception {
		
		String client_id = null, client_secret = null, refresh_token=null;
		
		JsonParser jsonParser = JSON_FACTORY.createJsonParser(new FileReader(new File("conf/"+CLIENT_REFRESH)));
		while (jsonParser.nextToken() != JsonToken.END_OBJECT) {
			String token = jsonParser.getCurrentName();
			if ("clientId".equals(token)) {
				jsonParser.nextToken();
				client_id = jsonParser.getText();
			}
			if ("clientSecret".equals(token)) {
				jsonParser.nextToken();
				client_secret = jsonParser.getText();
			}
			if ("refreshToken".equals(token)) {
				jsonParser.nextToken();
				refresh_token = jsonParser.getText();
			}
		}
		
		Credential credential = new GoogleCredential.Builder()
	    .setClientSecrets(client_id, client_secret)
	    .setJsonFactory(JSON_FACTORY).setTransport(HTTP_TRANSPORT).build()
	    .setRefreshToken(refresh_token);
		
	    Directory service = new Directory.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential)
	        .setApplicationName("GooglePassword")
	        .build();

	    User gUser = service.users().get(user).execute();
	    
	    gUser.setHashFunction("SHA-1");
	    gUser.setPassword(DigestUtils.sha1Hex(password));
	    
	    service.users().update(user, gUser).execute();
	}

}
