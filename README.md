**Configuration et mise en place dans OpenIDM de l'application de changement de password Google :**

- Connectez vous avec un compte super-administrateur à la console développeurs de votre domaine : 
*https://www.google.com/a/votre_domaine*

- Créez un nouveau projet que vous appellerez *GooglePassword*

- En suivant le sous-menu *Api et Authentification / Api*, activez pour ce projet l'api *admin-sdk*

- En suivant le sous-menu *"Api et Authentification" / "Identifiants"*, créez un identifiant client avec les paramètres suivants :

```
#!text


Type d'application : Application installée
Type d'application installée : autre
```


- Téléchargez le JSON associé à l'identifiant client nouvellement créé (*client_secret.json*)

- Lancez en ligne de commande *GooglePassword.jar* en lui passant en paramètre le JSON téléchargé pour générer le fichier de configuration *google_config.json* :


```
#!shell

java -jar GooglePassword.jar client_secret.json
```


**remarque** : La commande ci-dessus vous demandera de consulter une page de consentement où vous copierez un token que vous collerez dans notre application. Le fichier *google_config.json* sera créé dans le répertoire courant. Si tel n'est pas le cas, créez le et mettez-y le contenu JSON affiché par *GooglePassword.jar*

- Arrêtez OpenIDM

- Copiez le fichier *google_config.json* généré plus tôt dans le répertoire *conf/* d'OpenIDM.

- Copiez *GooglePassword.jar* dans le répertoire *bundle/* d'OpenIDM.

- Appelez comme suit la méthode de changement de password à partir du fichier javascript d'update qui convient :


```
#!javascript

try {
        org.janua.GooglePassword.changePassword(source.email , source.password);
        logger.info("GooglePassword : password changed for user {}", source.email);
} catch (err) {
        logger.error("GooglePassword Exception: {}", err);
}

```

**remarque** : le fichier d'update ainsi que les paramètres email et password dépendent évidemment de votre configuration OpenIDM.

- Redémarrez OpenIDM